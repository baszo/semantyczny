var http = require('http');
var fs = require("fs");
var querystring = require('querystring');
var utils = require('utils');
var openWeatherMapKey = "3804203cf90f9dc1ac4533c2e7c8893a";
var request = require("request");
var exec = require('child_process').exec;
var child;

function GetData(city)
{
var sucesss = true;
var requestString = "http://api.openweathermap.org/data/2.5/weather?q="
                    + city
                    + "&cluster=yes&mode=xml"
                    + "&APPID=" + openWeatherMapKey;

request(requestString, function(error, response, body) {
	  console.log("asdasd-"+body);
	  body = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"+body;
	  fs.writeFile("data/"+city+".xml", body, function(err) {
		    if(err) {
		        return console.log(err);
		    }

		    console.log("The file was saved!");
		    child = exec("xmllint --noout --dtdvalid data/current.dtd data/Krakow.xml", function (error, stdout, stderr) {

			  console.log('stdout: ' + stdout);

			  console.log('stderr: ' + stderr);

			  if (error !== null) {

			    console.log('exec error: ' + error);
			    sucesss=false;

			  }
			  if(stderr)
			  {
			  	sucesss=false;
			  }

			});
	  }); 
	});
return sucesss;
}


http.createServer(function(request, response) {

	console.log("[200] " + request.method + " to " + request.url);

	if (request.method == 'POST')
	{
		    console.log("[200] " + request.method + " to " + request.url);

		      
		    var fullBody = '';
		    
		    request.on('data', function(chunk) {
		      // append the current chunk of data to the fullBody variable
		      fullBody += chunk.toString();
		    });
		    
		    request.on('end', function() {
		      
		      // parse the received body data
		      var decodedBody = querystring.parse(fullBody);
		      console.log(decodedBody);
		      switch(decodedBody.typ){
		      	case "saveXML": 
		      		if(GetData(decodedBody.city))
		      			sendFileContent(response, "sucess.html", "text/html");
		      		else
		      			sendFileContent(response, "failed.html", "text/html");
		      	  break;
		      	default:
		      		sendFileContent(response, "index.html", "text/html");
		      }

		    });
		    return;
  	}

	if(request.url === "/index" || request.url === "/"){
		sendFileContent(response, "index.html", "text/html");
	}
	else if(/^\/[a-zA-Z0-9\/]*.js$/.test(request.url.toString())){
		sendFileContent(response, request.url.toString().substring(1), "text/javascript");
	}
	else if(/^\/[a-zA-Z0-9\/]*.css$/.test(request.url.toString())){
		sendFileContent(response, request.url.toString().substring(1), "text/css");
	}
	else{
		console.log("Requested URL is: " + request.url);
		response.end();
	}
}).listen(3000);

function sendFileContent(response, fileName, contentType){
	fs.readFile(fileName, function(err, data){
		if(err){
			response.writeHead(404);
			response.write("Not Found!");
		}
		else{
			response.writeHead(200, {'Content-Type': contentType});
			response.write(data);
		}
		response.end();
	});
}
