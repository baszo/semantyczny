<?php
// tworzymy obiekt klasy DOMImplementation
$imp = new DOMImplementation;

// tworzymy definicje typu dokumentu (DTD)
// pierwszy argument: nazwa elementu DTD
// drugi argument: pusty
// trzeci argument: nazwa pliku z dtd
// odpowiada to utworzeniu lini: <!DOCTYPE biblioteka SYSTEM "bib.dtd"> 
$dtd = $imp->createDocumentType('biblioteka','','bib.dtd');

// teraz tworzymy obiekt DOM razem z DTD
// pierwszy argument: URI (u nas pusty)
// drugi argument: pusty
// trzeci argument: odwolanie do zmiennej przechowujacej definicje DTD
$dom = $imp->createDocument('','', $dtd);

// ustawiam dodatkowe opcje na obiekcie DOM
$dom->encoding = 'UTF-8';
$dom->standalone = false;
$dom->formatOutput = TRUE; // to zagwarantuje wciecia w pliku

// dodaje element glowny
$root = $dom->createElement('biblioteka');
$root = $dom->appendChild($root);

$ksiazka = $dom->createElement('ksiazka');
$ksiazka = $root->appendChild($ksiazka);
$ksiazka->setAttribute('id','ampt');
$ksiazka->setAttribute('klasa','A');
$autor = $dom->createElement('autor');
$autor = $ksiazka->appendChild($autor);
$AutorValue = $dom->createTextNode('Adam Mickiewicz');
$AutorValue = $autor->appendChild($AutorValue);

$tytul = $dom->createElement('tytul');
$tytul = $ksiazka->appendChild($tytul);
$TytulValue = $dom->createTextNode('Pan Tadeusz');
$TytulValue = $tytul->appendChild($TytulValue);

$wydawca = $dom->createElement('wydawca');
$wydawca = $ksiazka->appendChild($wydawca);
$WydawcaValue = $dom->createTextNode('Wydawnictwo Znak');
$WydawcaValue = $wydawca->appendChild($WydawcaValue);

$rok = $dom->createElement('rok');
$rok = $ksiazka->appendChild($rok);
$RokValue = $dom->createTextNode('2009');
$RokValue = $rok->appendChild($RokValue);

$dom->SaveXML();
$status = $dom->save('domsave.xml');
if($status == TRUE){
        echo 'plik zapisamy';
}
else{
        echo 'Blad zapisu';
}
?>

