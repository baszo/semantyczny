<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="UTF-8" />
   <xsl:template match="/"> 
     <html> 
       <head>
       </head>
		<body>
       <h1>TEST</h1> 
         <xsl:for-each select="catalog/cd">
		 <xsl:sort select="artist" order="descending"/>
			<xsl:choose>
				<xsl:when test="price &gt; 10">
				<p style="border: 1px dotted;">
					Artist: <span style="color:red">
					<xsl:value-of select="artist"/></span> &#124; 
					Title: <span style = "color:green">
					<xsl:value-of select="title"/></span> &#124;
					Country: <span style = "color:violet">
					<xsl:value-of select="country"/></span> &#124;
					Company: <span style = "color:blue">
					<xsl:value-of select="company"/></span> &#124;
					Price: <span style = "color:pink">
					<xsl:value-of select="price"/></span> &#124;
					Year: <span style = "color:grey">
					<xsl:value-of select="year"/></span> &#124;
				</p>
				</xsl:when>
				<xsl:otherwise>
				<p>
					Artist: <span >
					<xsl:value-of select="artist"/></span> &#124; 
					Title: <span >
					<xsl:value-of select="title"/></span> &#124;
					Country: <span >
					<xsl:value-of select="country"/></span> &#124;
					Company: <span >
					<xsl:value-of select="company"/></span> &#124;
					Price: <span >
					<xsl:value-of select="price"/></span> &#124;
					Year: <span >
					<xsl:value-of select="year"/></span> &#124;
				</p>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
		</body>
     </html>

   </xsl:template>

</xsl:stylesheet>

