<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="UTF-8" />

	<xsl:template match="/">
        <html>
        <body>
	<ul style="font-size:17px;line-height:150%">
	 <xsl:apply-templates />  
        </ul>
       </body>
       </html>
	</xsl:template>

     <xsl:template match="speaker">
       <li>
         <b><xsl:value-of select="name"/></b> 
         - 
         <i><xsl:value-of select="institute"/></i>
       </li>
     </xsl:template>

</xsl:stylesheet>
