var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'Express' });
});

router.get('/test/:a,:b,:c,:d',function(req,res){
  var obj = {a:req.params.a, b:req.params.b, c:req.params.c, d:req.params.d};
  res.render('testowy',obj);
});

module.exports = router;
