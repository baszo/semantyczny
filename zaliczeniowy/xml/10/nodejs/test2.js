var js2xmlparser = require("js2xmlparser");

var data = {
			"firstName" : "John",
			"lastName": "Smith",
			"dateOfBirth": new Date(2014,01,01),
			"address": {
				"@": {
						"type" : "home"
				},
				"#": "Kraków"
			},
			"phone" : [
			{
				"@": { "type": "cell" },
				"#": "01-22044"
			},
			{
				"@": {"type" : "home"},
				"#": "98-98-98"
			}
			]
};

var xml = js2xmlparser("person",data);
console.log(xml);
