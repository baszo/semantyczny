<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="UTF-8" />
   <xsl:template match="/"> 
     <html> 
       <head>
       </head>
		<body>
       <h1>SALONIK</h1> 
		<table border="1">
         <tr> 
           <th>Marka</th> 
           <th>Model</th>
           <th>Rocznik</th> 
           <th>Numerkatalogowy</th>
           <th>Cena</th> 
         </tr>
         <xsl:for-each select="cennik/samochod">
		 <xsl:sort select="rocznik" order="descending"/>
			<tr>
				<xsl:choose>
					<xsl:when test="marka = 'VW'">
						<td style="font-weight:bold"><xsl:value-of select="marka"/></td>
					</xsl:when>
					<xsl:otherwise>
						<td><xsl:value-of select="marka"/></td>
					</xsl:otherwise>
				</xsl:choose>

					<td><xsl:value-of select="model"/></td>
					<td><xsl:value-of select="rocznik"/></td>
					<td><xsl:value-of select="numerkatalogowy"/></td>
				<xsl:choose>
					<xsl:when test="cena &gt; 50000">
							<td style="background:red"><xsl:value-of select="cena"/></td>

					</xsl:when>
					<xsl:when test="cena &lt; 50000 and cena &gt; 20000">

							<td style="background:yellow"><xsl:value-of select="cena"/></td>
					</xsl:when>
					<xsl:otherwise>

							<td style="background:green"><xsl:value-of select="cena"/></td>
					</xsl:otherwise>
				</xsl:choose>
			</tr>
		</xsl:for-each>

       </table>
		</body>
     </html>

   </xsl:template>

</xsl:stylesheet>

