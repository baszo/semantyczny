<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="UTF-8" />
   <xsl:template match="/"> 
     <html> 
       <head>
         <title>Nagrody Nobla w dziedzinie literatury</title>
		<link rel="stylesheet" type="text/css" href="uj.css" />
       </head>
       <h1>Nagrody Nobla w dziedzinie literatury</h1>
       <p>Liczba nagrod Nobla przyznanych Amerykanom w poszczegolnych
          dekadach dwudziestego wieku</p>
       <table border="1">
         <tr> 
           <th>Dziesieciolecie</th> 
           <th>Nagrody</th>
         </tr>

         <xsl:apply-templates /> 

       </table>
     </html>

   </xsl:template>

   <xsl:template match="prizes"> 
     <tr> 
       <td>
         <xsl:value-of select="@decade"/>
       </td>
       <td>
         <xsl:value-of select="@prizes"/>
       </td>
     </tr>
   </xsl:template>

</xsl:stylesheet>

